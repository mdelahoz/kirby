<?php snippet('header') ?>
<?php if($site->user()): ?>
  <main class="main" role="main">

   <!-- <h1><?php //echo $page->title()->html() ?></h1> -->

<!-- <img id="featureImg" src="http://cdainterview.com/resources/contact-us.png">-->

      <?php foreach($page->images()->sortBy('sort', 'asc') as $image): ?>
        <figure>
          <img src="<?= $image->url() ?>" alt="<?= $page->title()->html() ?>" style="width: 100%" />
        </figure>
      <?php endforeach ?>

<div id="padding">
<div class="message-text"><span style="font-size:17px; font-weight:bold; ">BeMo Academic Consulting Inc. </span><br /><span><span style="font-size:13px; font-weight:bold; "><u>Toll Free</u></span><span style="font-size:13px; ">: </span><span style="font-size:14px; ">1-855-900-BeMo (2366)</span><span style="font-size:13px; "><br /></span><span style="font-size:13px; font-weight:bold; "><u>Email</u></span><span style="font-size:13px; ">: </span><span style="font-size:14px; ">info@bemoacademicconsulting.com</span></div><br />

<form  method="post">

<?php if($alert): ?>
      <div class="alert">
        <ul>
          <?php foreach($alert as $message): ?>
          <li><?php echo html($message) ?></li>
          <?php endforeach ?>
        </ul>
      </div>
      <?php endif ?>

	 <div style="text-align: center!important;"> 
		<label>Name:</label> *<br />
		<input class="form-input-field" type="text" value="" id="name" name="name" required size="40"/><br /><br />

		<label>Email Address:</label> *<br />
		<input class="form-input-field" type="text" value="" id="email" name="email" required size="40"/><br /><br />

		<label>How can we help you?</label> *<br />
		<textarea class="form-input-field" id="text" name="text" required rows="8" cols="38"></textarea><br /><br />

		<div style="display: none;">
			<label>Spam Protection: Please don't fill this in:</label>
			<textarea name="comment" rows="1" cols="1"></textarea>
		</div>
		<input type="hidden" name="form_token" value="13128404285ada850138fea" />
		<input class="form-input-button" type="reset" name="resetButton" value="Reset" />
		<input class="form-input-button" type="submit" name="submit" value="Submit" />
	</div>
</form>

<br />
<div class="form-footer"><span style="font-size:15px; font-weight:bold; "><u>Note</u></span><span style="font-size:15px; ">: If you are having difficulties with our contact us form above, send us an email to info@bemoacademicconsulting.com (copy & paste the email address)</span><span style="font-size:13px; "><br /></span></div><br />

</div>
  </main>
<?php endif ?>
<?php snippet('footer') ?>
