<?php snippet('header') ?>
<?php if($site->user()): ?>
  <main class="main" role="main">
    

    <!-- <img id="featureImg" src="<?= url('assets/images/cda-interview-guide.jpg'); ?>"-->

      <?php
	foreach($page->images()->sortBy('sort', 'asc') as $image): ?>
        <figure>
          <img src="<?= $image->url() ?>" alt="<?= $page->title()->html() ?>" style="width: 100%"/>
        </figure>
      <?php endforeach ?>

    <div class="text wrap">
      <?= $page->text()->kirbytext() ?>
    </div>
 <!-- 
    <section class="projects-section">
      
      <div class="wrap wide">
        <h2>Latest Projects</h2>
        <?php snippet('showcase', ['limit' => 3]) ?>
        <p class="projects-section-more"><a href="<?= page('projects')->url() ?>" class="btn">show all projects &hellip;</a></p>
      </div>
      
    </section>
-->
  </main>
<?php endif ?>
<?php snippet('footer') ?>
