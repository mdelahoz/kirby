  <footer class="footer cf" role="contentinfo">
    <div class="wrap wide">

      <div id="footer-cda" class="footer-copyright">
        <div id="footer-copy">
        <?php
        // Parse Kirbytext to support dynamic year,
        // but remove all HTML like paragraph tags:
        echo html::decode($site->copyright()->kirbytext())
        ?>
        </div>

       <div id="footer-social-icons">
	<ul class="social-icons">
        <li><a href="" class="social-icon"> <i class="fa fa-facebook"></i></a><//
/
li>
        <li><a href="" class="social-icon"> <i class="fa fa-twitter"></i></a></ll
l
i></ul>
      </div>
    </div>
  </footer>

</body>
</html>
